<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
if (($_SERVER['HTTP_HOST'] == 'wachtigall.pl') || ($_SERVER['HTTP_HOST'] == 'www.wachtigall.pl')) {

}
elseif (($_SERVER['HTTP_HOST'] == 'wachtigall.kwiecien.biz') || ($_SERVER['HTTP_HOST'] == 'www.wachtigall.kwiecien.biz')) {

} else
{
    define('DB_NAME', 'wachtigall');

    /** MySQL database username */
    define('DB_USER', 'wachtigall');

    /** MySQL database password */
    define('DB_PASSWORD', '123');

    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8mb4');

    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');
}
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{llCu#mj)4V! `_N{ O[-Z>}Q Fh8=vH<m0v7^##gbEYwgK(~UZ,W1wx. jK[IYD');
define('SECURE_AUTH_KEY',  'pg-#|kx?!FD=RUC*lRw!qY@/B4DUid[3/VBVikJ:M77f;Zx@`f%UZ&wd$7HRd{~U');
define('LOGGED_IN_KEY',    'NI*/up2}_.Ik|Gq?<DD^=EF7j+LBQ=&FkLHjhzr$?rt|H>Pz.Vqv<{@N%3|DUAE4');
define('NONCE_KEY',        'iKxqGI6xXdHj)o`o:t?T]Rr(k=%][%.XXxoVw-|#7X37Y-K*B=fs*3_D/WUuhudm');
define('AUTH_SALT',        '^_ @&SoO?[6*ou ,{uSj-Wf}D9Tpw4 Zq5v];RBq*h`pd6:uwBK>(J@,WON(G61F');
define('SECURE_AUTH_SALT', 'of/YIj?|L_p6]$#M&EpIS*1]wJ.p1ICUUM[40S2Vp9PdmQLHF*Oq2pe.HPW4%+Yo');
define('LOGGED_IN_SALT',   '%3^%N3IkN*QtVL!oIN*4;/p^^0}Qkk[:+.:s.qR/t?X_r=&Z`1{btZcX{X5i9l Q');
define('NONCE_SALT',       'WD9x[?Pn]ckdntJcQiy.msgso#+7*IPJc~HOB/JwrU%Y:?i^QZV<7av^Aj~_TT!U');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');