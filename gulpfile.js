var gulp = require('gulp'),
    browserfiy = require('gulp-browserify'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify'),
    debowerify = require('debowerify'),
    minifyCSS = require('gulp-minify-css');

gulp.task('js', function () {
    gulp.src('assets/scripts/app.js')
        .pipe(browserfiy({
            transform: ['debowerify'],
            insertGlobals: false,
            debug: true
        }))
        .on('error', notify.onError({
            title: 'JS!',
            message: '<%= error.message %>'
        }))
        .pipe(rename('app.js'))
        .pipe(gulp.dest('public/assets/script'));
});

gulp.task('sass', function () {
    gulp.src('assets/styles/app.scss')
        .pipe(sass())
        .on('error', notify.onError({
            title: 'sass',
            message: '<%= error.message %>'
        }))
        .pipe(rename('app.css'))
        .pipe(gulp.dest('public/assets/style'));
});



gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('assets/scripts/*.js', ['js']).on('change', livereload.changed);

    // SASS livereload
    gulp.watch('assets/styles/*/*.scss', ['sass']).on('change', livereload.changed);

    gulp.watch('assets/styles/*.scss', ['sass']).on('change', livereload.changed);
});

gulp.task('release', function() {
    gulp.src('public/assets/script/app.js')
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest('public/assets/script/app.js'))

    gulp.src('public/wp-content/themes/wachtigall/style.css')
        .pipe(minifyCSS())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('public/wp-content/themes/wachtigall/style.css'))
});

gulp.task('default', ['js', 'sass', 'watch']);